﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Lab___Employee
{
    class Employees
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public DateTime StartDate { get; set; }
        public Departments Department { get; set; }
        

        public Employees()
        {
               
        }

        public string SayHelloAndName()
        {
            return "Hello! My name is " + FirstName;
        }

        //public string SayHelloToOthers() //function not complete
        //{
        //    return "Hello " + otherPErson + ", I'm " + name + ".";
        //}
    }
}
