﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Lab___Employee
{
    class Program
    {
        static void Main(string[] args)
        {
            Employees bob = new Employees
            {
                FirstName = "Bob",
                LastName = "Johnson",
                JobTitle = "Programmer",
                StartDate = new DateTime(2010, 1, 16),
                Department = new Departments("IT")
            };

            Employees sally = new Employees
            {
                FirstName = "Sally",
                LastName = "Rally",
                JobTitle = "Saleswoman",
                StartDate = new DateTime(2009, 4, 30),
                Department = new Departments("Sales")
            };

            
            Console.WriteLine(sally.SayHelloAndName());
            Console.WriteLine(bob.SayHelloAndName());
            Console.ReadLine(); 
        }
    }
}
