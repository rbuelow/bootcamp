﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Lab___Employee
{
    class Departments
    {
        public string Name { get; set; }
        public string Location { get; set; }

        public Departments(string name)
        {
            Name = name;
        }
    }

}
