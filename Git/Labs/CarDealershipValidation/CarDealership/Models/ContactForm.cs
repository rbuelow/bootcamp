﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarDealership.Models
{
    public class ContactForm : IValidatableObject
    {
        public string Name { get; set; }
        public int PurchaseTimeFrameInMonths { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public decimal? Income { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (Income == null)
            {
                errors.Add(new ValidationResult("You must enter your income", new string[] { "Income" }));
            }

            if (Email == null)
            {
                errors.Add(new ValidationResult("You must enter an email address", new string[] { "Email" }));
            }

            if (Email != null && !Email.Contains("@"))
            {
                errors.Add(new ValidationResult("Email address must contain the '@' symbol", new string[] {"Email"}));
            }

            if (PhoneNumber == null)
            {
                errors.Add(new ValidationResult("You must enter a phone number", new string[] {"PhoneNumber"}));
            }

            if (PhoneNumber != null && PhoneNumber.Substring(0, 1) == "1")
            {
                if (PhoneNumber.Length == 14)
                {
                    if (PhoneNumber.Substring(1, 1) != "-" && PhoneNumber.Substring(5, 1) != "-" &&
                        PhoneNumber.Substring(9, 1) != "-")
                    {
                        errors.Add(new ValidationResult("You must enter a phone number in correct format",
                            new string[] {"PhoneNumber"}));
                    }
                }
                else
                {
                    if (PhoneNumber.Length != 11)
                    {
                        errors.Add(new ValidationResult("You must enter a phone number in correct format",
                            new string[] { "PhoneNumber" }));  
                    }
                }
            }
            else
            {
                errors.Add(new ValidationResult("You must enter a phone number in correct format", new string[] { "PhoneNumber" }));
            }

            if (Name == null)
            {
                errors.Add(new ValidationResult("You must enter a name", new string[] { "Name" }));
            }

            if (PurchaseTimeFrameInMonths > 12 && Income < 10000)
            {
                errors.Add(new ValidationResult("We don't want your business!!"));
            }

            return errors;
        }
    }
}