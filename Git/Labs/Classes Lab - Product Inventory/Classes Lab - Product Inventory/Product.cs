﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Lab___Product_Inventory
{
    class Product
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public int UnitPrice { get; set; }
        public string Color { get; set; }
        public int UnitsInStock { get; set; }

        
        //public void CurrentPrice(Product p)
        //{
        //    Console.WriteLine("Current price of {0} is: {1}", p, p.UnitPrice);
        //}
    }

    class Jeans : Product
    {
        public string WaistSize { get; set; }
        public string LengthSize { get; set; }

        //public Jeans()
        //{
        //    List<Jeans> listOfJeans = new List<Jeans>();
        //} 
    }

    class Shirt : Product
    {
        public string ShirtSize { get; set; }

        //public Shirt()
        //{
        //    List<Shirt> listOfShirts = new List<Shirt>();
        //}   
    }

    class Coat : Product
    {
        public string CoatSize { get; set; }

        //public Coat()
        //{
        //    List<Coat> listOfJeans = new List<Coat>();
        //}  
    }
}
