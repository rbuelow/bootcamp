﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Lab___Product_Inventory
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Inventory inventory = new Inventory();

            //do loop for adding products to inventory. User can exit loop when done and move onto edit unit stock below
            do
            {
                Console.WriteLine("Is this product a (S)hirt, (J)eans, or (C)oat?");
                Console.WriteLine("Type any other letter to edit unit stock.");
                var productType = Console.ReadLine().ToUpper();
                if (productType == "S")
                {
                    //user input for adding shirts to inventory
                    Shirt newShirt = new Shirt();

                    Console.WriteLine("What is the item's title?");
                    newShirt.Title = Console.ReadLine();
                    Console.WriteLine("What is the item's description");
                    newShirt.Description = Console.ReadLine();
                    Console.WriteLine("What is the item's price?");
                    newShirt.UnitPrice = int.Parse(Console.ReadLine());
                    Console.WriteLine("What is the item's color?");
                    newShirt.Color = Console.ReadLine();
                    Console.WriteLine("What size is the shirt?");
                    newShirt.ShirtSize = Console.ReadLine();
                    Console.WriteLine("How many units will be added to stock?");
                    newShirt.UnitsInStock = int.Parse(Console.ReadLine());

                    //add product info to inventory (list)
                    inventory.ListOfProducts.Add(newShirt);
                }
                else if (productType=="J")
                {
                    //user input for adding jeans to inventory
                    Jeans newJeans = new Jeans();

                    Console.WriteLine("What is the item's title?");
                    newJeans.Title = Console.ReadLine();
                    Console.WriteLine("What is the item's description");
                    newJeans.Description = Console.ReadLine();
                    Console.WriteLine("What is the item's price?");
                    newJeans.UnitPrice = int.Parse(Console.ReadLine());
                    Console.WriteLine("What is the item's color?");
                    newJeans.Color = Console.ReadLine();
                    Console.WriteLine("What is the waist size?");
                    newJeans.WaistSize = Console.ReadLine();
                    Console.WriteLine("How long are the jeans?");
                    newJeans.LengthSize = Console.ReadLine();
                    Console.WriteLine("How many units will be added to stock?");
                    newJeans.UnitsInStock = int.Parse(Console.ReadLine());

                    //add product info to inventory (list)
                    inventory.ListOfProducts.Add(newJeans);   
                }
                else if (productType == "C")
                {
                    //user input for adding Coats to inventory
                    Coat newCoat = new Coat();

                    Console.WriteLine("What is the item's title?");
                    newCoat.Title = Console.ReadLine();
                    Console.WriteLine("What is the item's description");
                    newCoat.Description = Console.ReadLine();
                    Console.WriteLine("What is the item's price?");
                    newCoat.UnitPrice = int.Parse(Console.ReadLine());
                    Console.WriteLine("What is the item's color?");
                    newCoat.Color = Console.ReadLine();
                    Console.WriteLine("What size is the coat?");
                    newCoat.CoatSize = Console.ReadLine();
                    Console.WriteLine("How many units will be added to stock?");
                    newCoat.UnitsInStock = int.Parse(Console.ReadLine());

                    //add product info to inventory (list)
                    inventory.ListOfProducts.Add(newCoat);
                }
                else
                {
                    break;
                }
            } while (true);

            //Start of editing products here
            Console.Clear();
            Console.WriteLine("You can edit units in stock for each product here!");
            Console.WriteLine("Hit any key to start");
            Console.ReadLine();
            foreach (var product in inventory.ListOfProducts)
            {
                Console.WriteLine("{0}: current units in stock --- {1}", product.Title, product.UnitsInStock); 
                Console.WriteLine("Do you want to edit the amount of units in stock?");
                var yesorno = Console.ReadLine();
                if (yesorno == "yes")
                {
                    Console.Clear();
                    Console.WriteLine("What is the updated units in stock for {0}?", product.Title);
                    int newUnitsInStock = int.Parse(Console.ReadLine());
                    product.UnitsInStock = newUnitsInStock;
                    Console.WriteLine("Units in stock for {0} is now {1}", product.Title, newUnitsInStock);
                }
                else
                {
                    continue;
                }
            }

            //display inventory stats here
            var totalInventoryValue = 0;
            foreach (var product in inventory.ListOfProducts)
            {
                var totalValue = product.UnitsInStock*product.UnitPrice;
                Console.WriteLine("Total value of {0}: {1:C}", product.Title, totalValue);
            }
            foreach (var product in inventory.ListOfProducts)
            {
                var totalValue = product.UnitsInStock * product.UnitPrice;
                totalInventoryValue += totalValue;
            }
            Console.WriteLine("Your inventory's total value is {0:C}", totalInventoryValue);
            Console.ReadLine();
        }
    }
}
