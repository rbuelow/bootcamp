﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorizor
{
    class Program
    {
        static void Main(string[] args)
        {   
            //comments.....
            Console.WriteLine("Please enter a number to factor:");
            var input = Console.ReadLine();
            var intInput = int.Parse(input);
            int totalNumberOfFactors = 0;
            Console.Clear();
            Console.WriteLine("The number you chose was " + input +". Hit enter to see the factors!");
            Console.ReadLine();
            Console.WriteLine("Factors of your number are:");
            
            // Loop for determining factors and how many factors
            for (int i = 1; i < intInput; i++)
            {
                if (intInput % i == 0)
                {
                    totalNumberOfFactors++;
                    Console.WriteLine(i);

                }
            }
            Console.WriteLine(Environment.NewLine + "The number of factors your number has is: " + totalNumberOfFactors + Environment.NewLine);
            
            // Is the number prime?
            if (totalNumberOfFactors == 1)
            {
                Console.WriteLine(input + " is a prime number.");
            }
            else 
                Console.WriteLine(input + " is NOT a prime number.");
            
            // Is the number perfect?

            Console.ReadLine();
        }
    }
}
