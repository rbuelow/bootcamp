﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Lab___Car
{
    class Car
    {
        public string Color { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string Year { get; set; }
        public string NumOfDoors { get; set; }

        public Car()
        {
            Color = "Black ";
            Make = "Mazda ";
            Model = "Miata ";
            Year = "2000 ";
        }

        public string KnownSpecs()
        {
            return Color + Year + Make + Model;
        }
 
    }
}
