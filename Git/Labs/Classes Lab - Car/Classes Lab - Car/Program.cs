﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Lab___Car
{
    class Program
    {
        static void Main(string[] args)
        {
            Car myNewCar = new Car();
            Console.WriteLine("You just bought a brand new car. Here are the known specs:");
            
            //This line produces myNewCar's known specs (constructor) as a concatenated string
            Console.WriteLine(myNewCar.KnownSpecs());
            
            Console.WriteLine(Environment.NewLine);
            Console.WriteLine("What color would you like to paint the car?");
            
            //User chooses new color for car
            var newColor = Console.ReadLine();
            myNewCar.Color = newColor + " ";
            

            Console.WriteLine(Environment.NewLine);
            Console.WriteLine("Your car's known specs are now:");
            Console.WriteLine(myNewCar.KnownSpecs());
            
            Console.ReadLine();
        }
    }
}
