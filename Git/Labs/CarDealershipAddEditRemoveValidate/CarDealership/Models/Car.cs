﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;

namespace CarDealership.Models
{
    public class Car : IValidatableObject
    {
        public int Id { get; set; }

        [Required]
        public string Make { get; set; }

        [Required]
        public string Model { get; set; }

        public string Year { get; set; }
        public string ImageUrl { get; set; }

        [Required]
        public string Title { get; set; }

        public string Description { get; set; }
        public decimal Price { get; set; }


        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            List<ValidationResult> errors = new List<ValidationResult>();

            if (Year == null)
            {
                errors.Add(new ValidationResult("You must enter a year", new string[] { "Year" }));
            }

            return errors;
        }
    }
}