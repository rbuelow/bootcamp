﻿using CarDealership.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CarDealership.Models;

namespace CarDealership.Controllers
{
    public class CarController : Controller
    {
        ICarRepository _repo = new DBCarRepository();

        // GET: Car
        public ActionResult Index()
        {
            var cars = _repo.GetAllCars();
            return View(cars);
        }

        public ActionResult Details(int id)
        {
            var car = _repo.GetCarById(id);
            return View(car);
        }

        public ActionResult AddACar()
        {
            return View();
        }

        public ActionResult EditCar(int id)
        {
            var car = _repo.GetCarById(id);
            return View(car);
        }

        public ActionResult ConfirmRemove(int id)
        {
            var car = _repo.GetCarById(id);
            return View(car);
        }

        public ActionResult RemoveCar(int id)
        {
            _repo.DeleteCar(id);
            return RedirectToAction("Index");          
        }

        [HttpPost]
        public ActionResult AddACar(Car car)
        {
            if (ModelState.IsValid)
            {
                _repo.AddCar(car);
                return RedirectToAction("Index");
            }
            else
            {
                return View("AddACar");
            }           
        }

        [HttpPost]
        public ActionResult EditCar(Car car)
        {
            if (ModelState.IsValid)
            {
                _repo.EditCar(car);
                return RedirectToAction("Index");
            }
            else
            {
                return View("EditCar");
            }
        }
    }
}