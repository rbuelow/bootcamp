﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintToCurrency
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please input a number to change to currency:");
            string input = Console.ReadLine();
            Console.WriteLine(string.Format("{0:c}", Convert.ToDecimal(input)));
            
            Console.WriteLine("\a");
            Console.ReadLine();

        }
    }
}
