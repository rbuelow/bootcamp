﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DateTime_HowManyWednesdays
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter date:");
            var userDate = DateTime.Parse(Console.ReadLine());
            Console.WriteLine("Enter number:");
            var userNum = int.Parse(Console.ReadLine());
            var output = HowManyWednesdays(userDate, userNum);
            foreach (var c in output)
            {
                Console.WriteLine(c);   
            }
                
            Console.ReadLine();

        }

        public static Array HowManyWednesdays(DateTime date, int num)
        {
            var dayOfDateInput = date.DayOfWeek;
            var counter = 0;
            DateTime[] outputDates = new DateTime[num];
            
            while (counter != num)
            {
                if (date.DayOfWeek == DayOfWeek.Wednesday)
                {
                    outputDates[counter] = date;
                    counter++;   
                }
                date = date.AddDays(1);
            }
            return outputDates;
        }
    }
}
