﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Lab___Shapes_and_Perimeters
{
    class Rectangle : Shape
    {
        public override int Area(int height, int width)
        {
            return height*width;
        }

        public override int Perimeter(int height, int width)
        {
            return (height*2) + (width*2);
        }
    }
}
