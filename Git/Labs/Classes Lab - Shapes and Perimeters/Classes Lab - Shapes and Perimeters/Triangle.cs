﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Lab___Shapes_and_Perimeters
{
    class Triangle : Shape
    {
        public override int Area(int height, int baselength)
        {
            return (height*baselength)/2;
        }

        public override int Perimeter(int sidelength1, int sidelength2, int baselength)
        {
            return sidelength1 + sidelength2 + baselength;
        }

        public override int Area(int sl1, int sl2, int sl3)
        {
            var p = (sl1 + sl2 + sl3)/2;
            var areaoftri = Math.Sqrt(p*(p - sl1)*(p - sl2)*(p-sl3));
            var intarea = Convert.ToInt32(areaoftri);
            return intarea;
        }
    }
}
