﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Lab___Shapes_and_Perimeters
{
    class Program
    {
        static void Main(string[] args)
        {
            Circle cir = new Circle();
            //area and circumference of circle
            Console.WriteLine("Enter radius of circle:");
            var radius = Console.ReadLine();
            int intradius = Convert.ToInt32(radius);
            Console.WriteLine("Area of circle is: {0}",cir.Area(intradius));
            Console.WriteLine("Circumference of circle is: {0}", cir.Perimeter(intradius));
            Console.ReadLine();

            Rectangle rec = new Rectangle();
            //area and perimeter of rectangle
            Console.WriteLine("Enter height of rectangle:");
            var height = Console.ReadLine();
            int intheight = Convert.ToInt32(height);
            Console.WriteLine("Enter width of rectangle:");
            var width = Console.ReadLine();
            int intwidth = Convert.ToInt32(width);
            Console.WriteLine("Area of rectangle is: {0}", rec.Area(intheight, intwidth));
            Console.WriteLine("Perimeter of rectangle is: {0}", rec.Perimeter(intheight, intwidth));
            Console.ReadLine();

            Triangle tri = new Triangle();
            //area and perimeter of triangle
            Console.WriteLine("Enter the length of one side of a triangle:");
            var sl1 = Console.ReadLine();
            var intsl1 = Convert.ToInt32(sl1);
            Console.WriteLine("Enter the length of the second side of a triangle:");
            var sl2 = Console.ReadLine();
            var intsl2 = Convert.ToInt32(sl2);
            Console.WriteLine("Enter the third side length of the triangle:");
            var sl3 = Console.ReadLine();
            var intsl3 = Convert.ToInt32(sl3);

            Console.WriteLine("Area of triangle is: {0}", tri.Area(intsl1, intsl2, intsl3));
            Console.ReadLine();
        }
    }
}
