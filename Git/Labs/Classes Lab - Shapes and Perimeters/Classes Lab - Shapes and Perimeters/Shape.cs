﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Lab___Shapes_and_Perimeters
{
    public class Shape
    {
        public virtual int Area(int height, int width)
        {
            return 1;
        }

        public virtual int Area(int radius)
        {
            return 1;
        }

        public virtual int Area(int sl1, int sl2, int sl3)
        {
            return 1;
        }

        public virtual int Perimeter(int sidelength)
        {
            return 1;
        }

        public virtual int Perimeter(int sidelength, int sl2)
        {
            return 1;
        }

        public virtual int Perimeter(int sidelength, int sl2, int sl3)
        {
            return 1;
        }
    }
}
