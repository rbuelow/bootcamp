﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Lab___Shapes_and_Perimeters
{
    class Circle : Shape
    {
        public override int Area(int radius)
        {
            var areaoftriangle = Math.PI*(radius*radius);
            return Convert.ToInt32(areaoftriangle);
        }

        public override int Perimeter(int radius)
        {
            var circumference = 2 * Math.PI * radius;
            return Convert.ToInt32(circumference);
        }
    }
}
