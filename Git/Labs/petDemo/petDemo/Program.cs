﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace petDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Pets[] pets = new Pets[]
            {
                new Pets
                {
                    Name = "Woot",
                    Type = "dog",
                    Age = "8"
                },
                new Pets
                {
                    Name = "Angry Cat",
                    Type = "cat",
                    Age = "14"
                },
                new Pets
                {
                    Name = "Dash",
                    Type = "dog",
                    Age = "7"
                },
                new Pets
                {
                    Name = "Pi",
                    Type = "parrot",
                    Age = "2"
                },
            };

            var dogs = pets.Where(x => x.Type == "dog");
            foreach (var y in dogs)
            {
                Console.WriteLine(y.Name);  
            }
            
            Console.ReadLine();
        }
    }

    public class Pets
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Age { get; set; }
    }
}
