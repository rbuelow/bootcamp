﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLab_AddressBook
{
    class Address
    {
        private string _firstName;
        private string _lastName;
        private string _phoneNumber;
        private string _emailAddress;
        private string _streetAddress;
        private string _city;
        private string _state;
        private string _country;
        private string _zipCode;

        public void GetName(string first, string last)
        {
            _firstName = first;
            _lastName = last;
        }

        public void GetPhoneNum(string num)
        {
            _phoneNumber = num;
        }

        public void GetEmailAddress(string email)
        {
            _emailAddress = email;
        }

        public void GetAddress()
        {
            
        }
    }
}
