UPDATE Employee
	SET LastName = 'Green'
WHERE EmpID = 11

UPDATE Employee
	SET [Status] = 'External'
FROM Employee
	INNER JOIN Location
	ON Employee.LocationID = Location.LocationID
WHERE City = 'Spokane'

--Select *
--From Employee
--	INNER JOIN Location
--	ON Employee.LocationID = Location.LocationID
--WHERE City = 'Spokane'

SELECT *
FROM Location
WHERE City = 'Seattle'

UPDATE Location
	SET Street = '111 1st Ave'
WHERE City = 'Seattle'

Select *
From Employee
	INNER JOIN Location
	ON Employee.LocationID = Location.LocationID
	INNER JOIN [Grant]
	ON Employee.EmpID = [Grant].EmpID
WHERE City = 'Boston'

UPDATE [Grant]
	SET Amount = 20000
FROM Employee
	INNER JOIN Location
	ON Employee.LocationID = Location.LocationID
	INNER JOIN [Grant]
	ON Employee.EmpID = [Grant].EmpID
WHERE City = 'Boston'


