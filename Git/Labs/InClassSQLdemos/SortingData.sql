SELECT *
FROM [Grant]
Order By GrantName

SELECT *
FROM Employee
Order By HireDate DESC

SELECT ProductName, Category
FROM CurrentProducts
Order By RetailPrice DESC

SELECT *
FROM [Grant]
Order By Amount DESC, GrantName

SELECT FirstName, LastName, City 
FROM Employee
	LEFT OUTER JOIN Location
	On Employee.LocationID = Location.LocationID
Order By City

