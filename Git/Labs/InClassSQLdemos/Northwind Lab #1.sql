SELECT OrderDate, Country
FROM Orders, Customers
WHERE Country = 'Brazil'

SELECT Employees.FirstName, Employees.LastName, Territories.*, Region.RegionDescription
FROM Employees
	INNER JOIN EmployeeTerritories
	ON Employees.EmployeeID = EmployeeTerritories.EmployeeID
	INNER JOIN Territories
	ON EmployeeTerritories.TerritoryID = Territories.TerritoryID
	INNER JOIN Region 
	ON Territories.RegionID = Region.RegionID
WHERE Employees.City = 'Seattle'

SELECT Count (DISTINCT RegionDescription)
From Territories
	INNER JOIN Region
	ON Territories.RegionID = Region.RegionID

 
SELECT OrderID, SUM([Order Details].UnitPrice*[Order Details].Quantity-([Order Details].UnitPrice*[Order Details].Quantity)*[Order Details].Discount) AS Total	
FROM [Order Details]
GROUP BY OrderID

--SELECT OrderID, [Order Details].UnitPrice*[Order Details].Quantity-([Order Details].UnitPrice*[Order Details].Quantity)*[Order Details].Discount AS Total	
--FROM [Order Details]

SELECT Customers.CompanyName, SUM([Order Details].UnitPrice*[Order Details].Quantity-([Order Details].UnitPrice*[Order Details].Quantity)*[Order Details].Discount) AS Total	
FROM [Order Details]
	INNER JOIN Orders
	ON [Order Details].OrderID = Orders.OrderID
	INNER JOIN Customers
	ON Orders.CustomerID = Customers.CustomerID
GROUP BY Customers.CompanyName
ORDER BY CompanyName

--SELECT Customers.CompanyName, Orders.OrderID, [Order Details].UnitPrice*[Order Details].Quantity-([Order Details].UnitPrice*[Order Details].Quantity)*[Order Details].Discount AS Total 	
--FROM [Order Details]
--	INNER JOIN Orders
--	ON [Order Details].OrderID = Orders.OrderID
--	INNER JOIN Customers
--	ON Orders.CustomerID = Customers.CustomerID
--ORDER BY CompanyName

SELECT SUM([Order Details].UnitPrice*[Order Details].Quantity-([Order Details].UnitPrice*[Order Details].Quantity)*[Order Details].Discount) AS Total	
FROM [Order Details]


