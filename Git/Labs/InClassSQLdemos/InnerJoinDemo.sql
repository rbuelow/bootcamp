SELECT *
FROM EmployeeTerritories
	INNER JOIN Employees
	ON EmployeeTerritories.EmployeeID = Employees.EmployeeID
	INNER JOIN Territories
	ON EmployeeTerritories.TerritoryID = Territories.TerritoryID

SELECT CompanyName, OrderDate, ProductName
FROM Orders
	INNER JOIN Customers
	ON Orders.CustomerID = Customers.CustomerID
	INNER JOIN [Order Details]
	ON Orders.OrderID = [Order Details].OrderID
	INNER JOIN Products
	ON [Order Details].ProductID = Products.ProductID
WHERE Country = 'USA'

SELECT [Order Details].*, Orders.*
FROM [Order Details]
	INNER JOIN Orders
	ON [Order Details].OrderID = Orders.OrderID
	INNER JOIN Products
	ON [Order Details].ProductID = Products.ProductID
WHERE ProductName = 'Chai'




	
