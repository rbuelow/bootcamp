﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace Classes_Lab___Bank_Account_Manager
{
    class Account
    {
        public string Pin { get; set; }
        
        protected decimal Balance { get; set; }
    }

    class Savings : Account
    {
         
    }

    class Checking : Account
    {
        
    }
}

