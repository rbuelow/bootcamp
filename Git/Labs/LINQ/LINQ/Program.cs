﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace LINQ
{
    class Program
    {
        /* Practice your LINQ!
         * You can use the methods in Data Loader to load products, customers, and some sample numbers
         * 
         * NumbersA, NumbersB, and NumbersC contain some ints
         * 
         * The product data is flat, with just product information
         * 
         * The customer data is hierarchical as customers have zero to many orders
         */
        static void Main()
        {
            //PrintOutOfStock();
            //PrintInStockMoreThanThreeDollars();
            //PrintWashingtonCutomersAndOrders();
            //PrintProductNames();
            PrintProductsAnd25PercentPrices();
            //PrintProductNamesUppersCase();
            //PrintProductsEvenNumberOfStock();
            //PrintNameCategoryPrice();
            //PrintPairs();
            //PrintCustomerOrderIdsAndTotalLessThan500();
            //PrintFirstThree();
            //PrintFirstThreeOrdersWa();

            Console.ReadLine();
        }
        //1. Find all products that are out of stock.
        private static void PrintOutOfStock()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(p => p.UnitsInStock == 0);

            Console.WriteLine("Exercise #1 - Products out of stock" + Environment.NewLine);
            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
            Console.WriteLine(Environment.NewLine);
        }
        //2. Find all products that are in stock and cost more than 3.00 per unit.
        static void PrintInStockMoreThanThreeDollars()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(i => i.UnitsInStock > 0 && i.UnitPrice > 3.00m);

            Console.WriteLine("Exercise #2 - Products in stock above $3.00" + Environment.NewLine);
            foreach (var product in results)
            {
                Console.WriteLine(product.ProductName);
            }
        }
        //3. Find all customers in Washington, print their name then their orders. (Region == "WA")
        static void PrintWashingtonCutomersAndOrders()
        {
            var products = DataLoader.LoadCustomers();

            var results = products.Where(i => i.Region == "WA");

            foreach (var customer in results)
            {
                Console.WriteLine(customer.CompanyName);
                foreach (var order in customer.Orders)
                {
                    var order1 = order.OrderID;
                    var order2 = order.OrderDate;
                    var order3 = order.Total;
                    Console.WriteLine("{0, -6} {1, -11:d} {2:C}", order1, order2, order3);
                }
                
            }
        }
        //4. Create a new sequence with just the names of the products.
        static void PrintProductNames()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Select(x => x.ProductName);

            foreach (var productName in results)
            {
                Console.WriteLine(productName);  
            }
        }
        //5. Create a new sequence of products and unit prices where the unit prices are increased by 25%.
        static void PrintProductsAnd25PercentPrices()
        {
            var products = DataLoader.LoadProducts();

            var productAndPriceResults = products.Select(x => new {x.ProductName, x.UnitPrice});

            foreach (var c in productAndPriceResults)
            {
                Console.WriteLine("{0, -33} {1:C}", c.ProductName, c.UnitPrice*1.25m);  
                //Console.WriteLine(c.ProductName);
                //Console.WriteLine(c.UnitPrice*1.25m);
            }
        }
        //6. Create a new sequence of just product names in all upper case.
        static void PrintProductNamesUppersCase()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Select(x => x.ProductName);

            foreach (var productName in results)
            {
                Console.WriteLine(productName.ToUpper());
            }
        }
        //7. Create a new sequence with products with even numbers of units in stock.
        static void PrintProductsEvenNumberOfStock()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Where(x => x.UnitsInStock % 2 == 0);

            foreach (var c in results)
            {
                
                Console.WriteLine("{0, -33} {1:####}",c.ProductName, c.UnitPrice);  
            }
        }
        //8. Create a new sequence of products with ProductName, Category, and rename UnitPrice to Price.
        static void PrintNameCategoryPrice()
        {
            var products = DataLoader.LoadProducts();

            var results = products.Select(x => new {x.ProductName, x.Category, Price = x.UnitPrice});

            foreach (var c in results)
            {
                Console.WriteLine("{0, -33} {1, -15} {2:C}", c.ProductName, c.Category, c.Price); 
            }

        }
        //9. Make a query that returns all pairs of numbers from both arrays such that the number from numbersA is 
        //less than the number from numbersB.
        static void PrintPairs()
        {
            var arrA = DataLoader.NumbersA;
            var arrB = DataLoader.NumbersB;

            var results = from a in arrA
                from b in arrB
                where a < b
                select new {a, b};
            foreach (var x in results)
            {
                Console.WriteLine(x.a + ", " + x.b);  
            }

        }
        //10. Select CustomerID, OrderID, and Total where the order total is less than 500.00.
        static void PrintCustomerOrderIdsAndTotalLessThan500()
        {
            var orderInfo = DataLoader.LoadCustomers();

            var results = from customer in orderInfo
                from id in customer.Orders
                where id.Total < 500.00m
                select new {customer.CustomerID, id.OrderID, id.Total};

            foreach (var x in results)
            {
                Console.WriteLine("{0} {1} {2:C}", x.CustomerID, x.OrderID, x.Total); 
            }

        }
        //11. Write a query to take only the first 3 elements from NumbersA.
        static void PrintFirstThree()
        {
            var arrA = DataLoader.NumbersA;

            var results = arrA.Take(3);
            
            foreach (var c in results)
            {
                Console.WriteLine(c); 
            }
        }
        //12. Get only the first 3 orders from customers in Washington.
        static void PrintFirstThreeOrdersWa()
        {
            var orderInfo = DataLoader.LoadCustomers();

            var results = ( from customer in orderInfo
                            from date in customer.Orders
                            where customer.Region == "WA"
                            select new {customer.CompanyName, date.OrderID, date.OrderDate})
                            .Take(3);

            foreach (var ordProp in results)
            {
                Console.WriteLine("{0:d} {1, -33} {2}", ordProp.OrderDate, ordProp.CompanyName, ordProp.OrderID);
            }
        }
    }
}
