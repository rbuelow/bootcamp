﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsLab___GuessingGame2._0
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rando = new Random();
            int numberToGuess = rando.Next(1, 21);
            string input = "";
            
            do
            {
                Console.WriteLine("Guess a number:");
                input = Console.ReadLine();
            } while (!ValidateNumber(input));

            int inputtedNumber = int.Parse(input);

            if (numberToGuess == inputtedNumber)
            {
                Console.WriteLine("You Won!!");
            }
            else if (numberToGuess > inputtedNumber)
            {
                Console.WriteLine("Too high!"); 
            }
            else if (numberToGuess < inputtedNumber)
            {
                Console.WriteLine("Too low!!");
            }
            Console.ReadLine();
        }

        public static bool ValidateNumber(string strNumber)
        {
            int inputtedNumber;
            bool isANumber = int.TryParse(strNumber, out inputtedNumber);
            if (inputtedNumber < 1)
            {
                return false;
            } 
            else if (inputtedNumber > 20)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
