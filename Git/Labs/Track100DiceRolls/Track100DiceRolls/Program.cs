﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Track100DiceRolls
{
    class Program
    {
        static void Main()
        {
            int roll1 = 0;
            int roll2 = 0;
            int roll3 = 0;
            int roll4 = 0;
            int roll5 = 0;
            int roll6 = 0;
            
            // New Random instance
            Random numGen = new Random();

            // rolls dice 100 times and adds 1 to the roll variables every time that value of the die is rolled
            for (int i = 1; i <= 100; i++)
            {
                var diceThrow = numGen.Next(1, 7); //Console.WriteLine(diceThrow);
                
                switch (diceThrow)
                {
                    case 1:
                        roll1 += 1;
                        break;
                    case 2:
                        roll2 += 1;
                        break;
                    case 3:
                        roll3 += 1;
                        break;
                    case 4:
                        roll4 += 1;
                        break;
                    case 5:
                        roll5 += 1;
                        break;
                    case 6:
                        roll6 += 1;
                        break;
                } 
            }

            //Array to print showing how many times each value was rolled on the die
            string[] diceArray = new string[6];
            {
                diceArray[0] = "The dice rolled a 1: " + roll1 + " times.";
                diceArray[1] = "The dice rolled a 2: " + roll2 + " times.";
                diceArray[2] = "The dice rolled a 3: " + roll3 + " times.";
                diceArray[3] = "The dice rolled a 4: " + roll4 + " times.";
                diceArray[4] = "The dice rolled a 5: " + roll5 + " times.";
                diceArray[5] = "The dice rolled a 6: " + roll6 + " times.";
            }

            //prints the dice array
            foreach (var item in diceArray)
            {
                Console.WriteLine(item);
            }
            Console.ReadLine();
        }
    }
}
