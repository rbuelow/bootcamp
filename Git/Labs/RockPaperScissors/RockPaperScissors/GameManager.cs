﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissors
{
    /*
     * This is the game manager, it will track the computer and player choices
     * 
     * For the purposes of this game 1 is Rock, 2 is Paper, 3 is Scissors
     * 
     * It keeps track of wins, losses, and draws
     * 
     * A working solution is in the Solved folder, try not to look at it...
     * 
     * Finish this code!
     */

    public class GameManager
    {
        private bool _keepPlaying;
        private int _computerChoice;
        private int _playerChoice;
        private string _computerChoiceText;
        private string _playerChoiceText;
        private int _wins = 0;
        private int _losses = 0;
        private int _draws = 0;

        private Random rng = new Random();

        public void Play()
        {
            _keepPlaying = false;

            do
            {
                Console.Clear();
                DisplayStats();
                GetComputerChoice();
                GetPlayerChoice();
                DetermineResult();

                AskToKeepPlaying();
            } while (_keepPlaying);
        }

        private void DisplayStats()
        {
            Console.WriteLine("Wins: {0}", _wins);
            Console.WriteLine("Losses: {0}", _losses);
            Console.WriteLine("Draws: {0}", _draws);
            //TODO: also print losses and draws - DONE
            Console.WriteLine();
        }

        private void GetComputerChoice()
        {       
            _computerChoice = rng.Next(1, 4);
            switch (_computerChoice)
            {
                //TODO: Add cases for rock and paper -DONE
                case 1:
                    _computerChoiceText = "Rock";
                    break;
                case 2:
                    _computerChoiceText = "Paper";
                    break;
                case 3:
                    _computerChoiceText = "Scissors";
                    break;
            }
        }

        private void GetPlayerChoice()
        {
            bool validChoice = false;

            Console.WriteLine("Enter your choice!");
            Console.WriteLine("------------------");
            while (!validChoice)
            {
                Console.Write("R for Rock, P for Paper, S for Scissors: ");
                string choice = Console.ReadLine();

                switch (choice)
                {
                    case "R":
                        validChoice = true;
                        _playerChoiceText = "Rock";
                        _playerChoice = 1;
                        break;
                    case "P":
                        validChoice = true;
                        _playerChoiceText = "Paper";
                        _playerChoice = 2;
                        break;
                    case "S":
                        validChoice = true;
                        _playerChoiceText = "Scissors";
                        _playerChoice = 3;
                        break;
                    //TODO: add cases for paper and scissors - DONE
                    default:
                        Console.WriteLine("That was not a valid choice!");
                        break;
                }
            }
        }

        private void DetermineResult()
        {
            Console.WriteLine();
            Console.WriteLine("You picked {0}, computer picked {1}", _playerChoiceText, _computerChoiceText);

            if (_playerChoice == _computerChoice)
            {
                Console.WriteLine("It's a draw!!");
                _draws++;
                return;
            }
            //if player chose rock, determines winner versus paper and scissors
            else if (_playerChoice == 1)
            {
                if (_computerChoice == 2)
                {
                    Console.WriteLine("You lost!!");
                    _losses++;
                    return;
                }
                else
                {
                    Console.WriteLine("You won!!");
                    _wins++;
                    return;
                }
            }
            //if player chose paper, determiness winner versus rock and scissors
            else if (_playerChoice == 2)
            {
                if (_computerChoice == 1)
                {
                    Console.WriteLine("You won!!");
                    _wins++;
                    return;
                }
                else
                {
                    Console.WriteLine("You lost!!");
                    _losses++;
                    return;
                }
            }
            //if player chose scissors, determines winner versus rock and paper
            else 
            {
                if (_computerChoice == 1)
                {
                    Console.WriteLine("You lost!!");
                    _losses++;
                    return;
                }
                else
                {
                    Console.WriteLine("You won!!");
                    _wins++;
                    return;
                }
            }
            //TODO: Add code to figure out whether they won or lost
        }

        private void AskToKeepPlaying()
        {
            Console.WriteLine();
            Console.Write("Play again (Y/N)? ");
            string response = Console.ReadLine();

            if (response == "Y")
            {
                _keepPlaying = true;
            }
            else
            {
                _keepPlaying = false;
            }
        }
    }
}
