﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsLab__TrackTwoDiceRolls
{
    class Dice
    {
        Random numGen = new Random();

        public int DiceRoll()
        {
            int randomDiNum = numGen.Next(0, 7);

            return randomDiNum;
        }

        public int RollTwoDice()
        {
            var sumOfTwoDice = DiceRoll() + DiceRoll();
            return sumOfTwoDice;
        }




    }
}
