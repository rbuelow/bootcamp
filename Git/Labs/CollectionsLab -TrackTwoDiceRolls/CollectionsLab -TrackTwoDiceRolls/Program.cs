﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsLab__TrackTwoDiceRolls
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> resultsOfRolls = new List<int>();
            Dice sumTwoDice = new Dice();

            // Simulates 100 rolls of two di
            for (int i = 0; i < 100; i++)
            {                
                var oneRoll = sumTwoDice.RollTwoDice();
                resultsOfRolls.Add(oneRoll);
            }

            //variable lists of each total result
            var total2 = resultsOfRolls.Where(i => i == 2);
            var total3 = resultsOfRolls.Where(i => i == 3);
            var total4 = resultsOfRolls.Where(i => i == 4);
            var total5 = resultsOfRolls.Where(i => i == 5);
            var total6 = resultsOfRolls.Where(i => i == 6);
            var total7 = resultsOfRolls.Where(i => i == 7);
            var total8 = resultsOfRolls.Where(i => i == 8);
            var total9 = resultsOfRolls.Where(i => i == 9);
            var total10 = resultsOfRolls.Where(i => i == 10);
            var total11 = resultsOfRolls.Where(i => i == 11);
            var total12 = resultsOfRolls.Where(i => i == 12);

            //Co'nsole.WriteLine(resultsOfRolls.Count);

            Console.WriteLine("A total of 2 was rolled {0} times.", total2.Count());
            Console.WriteLine("A total of 3 was rolled {0} times.", total3.Count());
            Console.WriteLine("A total of 4 was rolled {0} times.", total4.Count());
            Console.WriteLine("A total of 5 was rolled {0} times.", total5.Count());
            Console.WriteLine("A total of 6 was rolled {0} times.", total6.Count());
            Console.WriteLine("A total of 7 was rolled {0} times.", total7.Count());
            Console.WriteLine("A total of 8 was rolled {0} times.", total8.Count());
            Console.WriteLine("A total of 9 was rolled {0} times.", total9.Count());
            Console.WriteLine("A total of 10 was rolled {0} times.", total10.Count());
            Console.WriteLine("A total of 11 was rolled {0} times.", total11.Count());
            Console.WriteLine("A total of 12 was rolled {0} times.", total12.Count());

            Console.ReadLine();


        }   
    }
}
