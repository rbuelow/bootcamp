﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestDemo
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMathUtils()
        {
            MathUtils mathUtils =  new MathUtils();
            int result = mathUtils.Add(2, 2);
            Assert.AreEqual(4, result);
        }
    }
}
