﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringTimes
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Enter a word to repeat:");
            var word = Console.ReadLine();
            Console.WriteLine("Enter how many times to repeat your word:");
            var repeats = Console.ReadLine();
            var retval = StringTimes(word, int.Parse(repeats));
            Console.WriteLine(retval);
            Console.ReadLine();
        }

        public static string StringTimes(string str, int n)
        {
            string repeatstring="";
            for (int i = 0; i < n; i++)
            {
                repeatstring += str;
            }
            return repeatstring;
        }
    }
}
