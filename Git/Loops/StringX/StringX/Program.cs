﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace StringX
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(StringX("xxHxix"));
            Console.WriteLine(StringX("abxxxcd"));
            Console.WriteLine(StringX("xabxxxcdx"));
            Console.ReadLine();
        }

        public static string StringX(string str)
        {
            string noX = "";

            if (str.Substring(0,1)=="x")
            {
                noX += "x";
            }
                
            for (int i = 0; i < str.Length; i++)
            {

                if (str.Substring(i, 1) != "x")
                {
                    noX += str.Substring(i, 1);
                }
            }

            if (str.Substring(str.Length - 1, 1) == "x")
            {
                noX += "x";
            }
            
            return noX;
        }
    }
}
