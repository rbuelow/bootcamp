﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringSplosion
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a word:");
            var word = Console.ReadLine();
            var output = StringSplosion(word);
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static string StringSplosion(string str)
        {
            var wordAdder = "";
            for (int i = 0; i < str.Length; i++)
            {
                wordAdder += str.Substring(0, i + 1);
            }
            return wordAdder;
        }
    }
}
