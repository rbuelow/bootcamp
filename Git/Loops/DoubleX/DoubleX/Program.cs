﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoubleX
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a word:");
            var word = Console.ReadLine();
            var output = CountXX(word);
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static bool CountXX(string str)
        {
            var indexofx = str.IndexOf("x");
            if (str.Substring(indexofx, 2) == "xx")
            {
                return true;
            }
            return false;
        }
    }
}
