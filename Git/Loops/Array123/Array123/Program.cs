﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array123
{
    class Program
    {
        static void Main(string[] args)
        {
            //Array123({1, 1, 2, 3, 1}) -> true
            //Array123({1, 1, 2, 4, 1}) -> false
            //Array123({1, 1, 2, 1, 2, 3}) -> true
            int[] arr1 = new[] {1, 1, 2, 3, 1};
            int[] arr2 = new[] {1, 1, 2, 4, 1};
            int[] arr3 = new[] {1, 1, 2, 1, 2, 3};

            Console.WriteLine(Array123(arr1));
            Console.WriteLine(Array123(arr2));
            Console.WriteLine(Array123(arr3));
            Console.ReadLine();
        }

        public static bool Array123(int[] numbers)
        {
            bool oneTwoThree = false;

            for (int i = 0; i < numbers.Length - 2; i++)
            {
                
                if (numbers[i] == 1)
                {
                    if (numbers[i + 1] == 2 && numbers[i + 2] == 3)
                    {
                        oneTwoThree = true;
                    }
                }   
            }
            return oneTwoThree;
        }
    }
}
