﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayFront9
{
    class Program
    {
        //Given an array of ints, return true if one of the first 4 elements in the array is a 9. The array length may be less than 4. 

        //ArrayFront9({1, 2, 9, 3, 4}) -> true
        //ArrayFront9({1, 2, 3, 4, 9}) -> false
        //ArrayFront9({1, 2, 3, 4, 5}) -> false
        
        static void Main(string[] args)
        {
            int[] arr1 = new[] {1, 2, 9, 4};
            int[] arr2 = new[] {1, 2, 3, 4, 5, 6, 9};
            int[] arr3 = new[] {9};

            Console.WriteLine(ArrayFront9(arr1));
            Console.WriteLine(ArrayFront9(arr2));
            Console.WriteLine(ArrayFront9(arr3));
            Console.ReadLine();
        }

        public static bool ArrayFront9(int[] numbers)
        {
            bool isThereANine = false;
            if (numbers.Length > 4)
            {
                for (int i = 0; i < 4; i++)
                {
                    if (numbers[i] == 9)
                    {
                        isThereANine = true;
                        break;
                    }
                    else
                    {
                        isThereANine = false;
                    }
                }
            }
            else
            {
                for (int i = 0; i < numbers.Length; i++)
                {
                    if (numbers[i] == 9)
                    {
                        isThereANine = true;
                        break;
                    }
                    else
                    {
                        isThereANine = false;
                    }
                }   
            }
            return isThereANine;
        }
    }
}
