﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontTimes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a word:");
            var word = Console.ReadLine();
            Console.WriteLine("Enter how many times to repeat:");
            var repeats = Console.ReadLine();
            var output = FrontTimes(word, int.Parse(repeats));
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static string FrontTimes(string str, int n)
        {
            string repeatstring = "";
            for (int i = 0; i < n; i++)
            {
                if (str.Length >= 3)
                {
                    repeatstring += str.Substring(0, 3);
                }
                else
                {
                    repeatstring += str;
                }
            }
            return repeatstring;
        }
    }
}
