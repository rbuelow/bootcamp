﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Array667
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = { 6, 6, 2 };
            int[] arr2 = { 6, 6, 2, 6 };
            int[] arr3 = { 6, 7, 2, 6, 6 };
            Console.WriteLine(Array667(arr1));
            Console.WriteLine(Array667(arr2));
            Console.WriteLine(Array667(arr3));
            Console.ReadLine();
        }

        public static int Array667(int[] numbers)
        {
            int counter = 0;
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                if ((numbers[i] == 6 && numbers[i + 1] == 7) || (numbers[i] == 6 && numbers[i + 1] == 6))
                {
                    counter++;
                }
            }
            return counter;
        }
    }
}
