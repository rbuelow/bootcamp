﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubStringMatch
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(SubStringMatch("xxcaazz", "xxbaaz"));
            Console.WriteLine(SubStringMatch("abc", "abc"));
            Console.WriteLine(SubStringMatch("abc", "axc"));
            Console.ReadLine();
        }

        public static int SubStringMatch(string a, string b)
        {
            int counter = 0;
            for (int i = 0; i < Math.Min(a.Length - 1, b.Length - 1); i++)
            {
                if (a.Substring(i, 2) == b.Substring(i, 2))
                {
                    counter++;
                }
            }
            return counter;
        }
    }
}
