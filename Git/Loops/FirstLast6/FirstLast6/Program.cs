﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstLast6
{
    class Program
    {
        //Given an array of ints, return true if 6 appears as either the first or last element in the array. The array will be length 1 or more. 

        //FirstLast6({1, 2, 6}) -> true
        //FirstLast6({6, 1, 2, 3}) -> true
        //FirstLast6({13, 6, 1, 2, 3}) -> false

        //public bool FirstLast6(int[] numbers) {

        //}
        static void Main(string[] args)
        {
            int[] arr1 = new[] {1, 4, 6};
            int[] arr2 = new[] {3, 8, 9};

            var output1 = FirstLast6(arr1);
            var output2 = FirstLast6(arr2);

            Console.WriteLine(output1);
            Console.WriteLine(output2);
            Console.ReadLine();
        }

        public static bool FirstLast6(int[] numbers)
        {
            if (numbers[0] == 6 || numbers[2] == 6)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
