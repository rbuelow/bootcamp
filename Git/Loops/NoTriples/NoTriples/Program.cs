﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoTriples
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = new[] { 1, 1, 2, 2, 1 };
            int[] arr2 = new[] { 1, 1, 2, 2, 2, 1 };
            int[] arr3 = new[] { 1, 1, 2, 2, 1, 3, 4, 5, 6, 7, 8, 8, 8 };
            Console.WriteLine(NoTriples(arr1));
            Console.WriteLine(NoTriples(arr2));
            Console.WriteLine(NoTriples(arr3));
            Console.ReadLine();

        }

        public static bool NoTriples(int[] numbers)
        {
            bool triple = true;
            for (int i = 0; i < numbers.Length - 2; i++)
            {
                if (numbers[i] == numbers[i + 1] && numbers[i] == numbers[i + 2])
                {
                    triple = false;
                }
            }
            return triple;   
        }
    }
}
