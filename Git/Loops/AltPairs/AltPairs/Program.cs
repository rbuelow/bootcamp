﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AltPairs
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(AltPairs("kitten"));
            Console.WriteLine(AltPairs("Chocolate"));
            Console.WriteLine(AltPairs("CodingHorror"));
            Console.ReadLine();
        }

        public static string AltPairs(string str)
        {
            string altString = "";

            for (int i = 0; i < str.Length; i+=4)
            {
                if (i < str.Length - 1)
                {
                    altString += str.Substring(i, 2);
                }
                else
                {
                    altString += str.Substring(i, 1);
                }
            }

            return altString;
        }
    }
}
