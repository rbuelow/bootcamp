﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CountLast2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a word:");
            var word = Console.ReadLine();
            var output = CountLast2(word);
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static int CountLast2(string str)
        {
            int counter = 0;
            
            for (int i = 0; i < str.Length - 2; i++)
            {
                var lastTwo = str.Substring(str.Length - 2, 2);
                

                if (str.Substring(i, 2) == lastTwo)
                {
                    counter++;
                }
            }
            return counter;
        }
    }
}
