﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountXX
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Enter a word:");
            var word = Console.ReadLine();
            var output = CountXX(word);
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static string CountXX(string str)
        {
            var counter = 0;
            for (int i = 0; i < str.Length-1; i++)
            {
                var nameThis = str.Substring(i, 2);
                
                if (nameThis == "xx")
                {
                    counter++;
                }
                
            }
            return counter.ToString();
        }
    }
}
