﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pattern51
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = { 1, 2, 7, 1 };
            int[] arr2 = { 1, 2, 8, 1 };
            int[] arr3 = { 2, 7, 1 };
            Console.WriteLine(Pattern51(arr1));
            Console.WriteLine(Pattern51(arr2));
            Console.WriteLine(Pattern51(arr3));
            Console.ReadLine();
        }
        public static bool Pattern51(int[] numbers)
        {
            bool pattern = false;
            for (int i = 0; i < numbers.Length - 2; i++)
            {
                if (numbers[i] == numbers[i + 1] - 5 && numbers[i] == numbers[i + 2] + 1)
                {
                    pattern = true;
                }
            }
            return pattern;
        }
    }
}
