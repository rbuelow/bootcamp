﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EveryOther
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a word:");
            var word = Console.ReadLine();
            var output = EveryOther(word);
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static string EveryOther(string str)
        {
            var oddstr = "";
            for (int i = 0; i < str.Length - 1; i++)
            {
                
                if (i%2 == 0)
                {
                    oddstr += str.Substring(i, 1);
                }
                else
                {
                    continue;   
                }
            }
            return oddstr;
        }
    }
}
