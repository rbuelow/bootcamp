﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstHalf
{
    class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Please input a word with an even amount of characters:");
            var input = Console.ReadLine();
            var inputlength = input.Length;
            if (inputlength%2 == 0)
            {
                Console.WriteLine(FirstHalf(input));
            }
            else
            {
                Console.WriteLine("This word does not have an even amount of characters.");
            }
            Console.ReadLine();
        }

        public static string FirstHalf(string str)
        {
            var half = str.Length/2;
            var fh = str.Substring(0, half);
            return fh;
        }
    }
}
