﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakeTags
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Provide which HTML tag you'd like:");
            var htmltag = Console.ReadLine();
            Console.WriteLine("Provide what text content you'd like:");
            var text = Console.ReadLine();
            Console.WriteLine(MakeTags(htmltag,text));
            Console.ReadLine();
        }

        public static string MakeTags(string tag, string content)
        {
            return "<" + tag + ">" + content + "</" + tag + ">";
        }
    }
}
