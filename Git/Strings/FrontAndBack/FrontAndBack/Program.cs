﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontAndBack
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input a word:");
            var word = Console.ReadLine();
            Console.WriteLine("Input a number:");
            var number  = Console.ReadLine();
            var answer = FrontAndBack(word, Int32.Parse(number));
            Console.WriteLine(answer);
            Console.ReadLine();
        }

        public static string FrontAndBack(string str, int n)
        {
            if (str.Length >= n)
            {
                var a = str.Substring(0, n);
                var b = str.Substring(str.Length - n, n);
                return a + b;
            }
            else
            {
                return "Your word is too short!";
            }
        }
    }
}
