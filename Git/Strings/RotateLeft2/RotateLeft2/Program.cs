﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RotateLeft2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input a word:");
            var word = Console.ReadLine();
            if (word.Length < 2)
            {
                Console.WriteLine("Word is not long enough");
            }
            else
                Console.WriteLine(Rotateleft3(word));

            Console.ReadLine();
        }

        public static string Rotateleft3(string str)
        {
            var beginning = str.Substring(0, 2);
            var end = str.Substring(2, str.Length-2);
            return end + beginning;
        }
    }
}
