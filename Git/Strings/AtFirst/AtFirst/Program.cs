﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input a word:");
            var word = Console.ReadLine();
            var output = AtFirst(word);
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static string AtFirst(string str)
        {
            if (str.Length >= 2)
            {
                return str.Substring(0, 2);
            }
            else if (str.Length == 1)
            {
                return str.Substring(0, 1) + "@";
            }
            else
            {
                return "Your word is not long enough!";
            }
        }
    }
}
