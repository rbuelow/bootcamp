﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadStringReverseOrder
{
    class Program
    {
        static void Main()
        {
            Program p = new Program();
            Console.WriteLine("Please enter a string to reverse:");
            var input = Console.ReadLine();
            var reversedString = p.ReverseString(input);
            Console.WriteLine(reversedString);
            Console.ReadLine();
        }

        public string ReverseString(string str)
        {
            char[] charArr = str.ToCharArray();
            char[] rCharArr = charArr.Reverse().ToArray();

            string s = "";
            foreach (char c in rCharArr)
            {
                s = s + c;
            }

            return s;

        }
    }
}
