﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace abba
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Please provide first word:");
            var firstword = Console.ReadLine();
            Console.WriteLine("Please provide second word:");
            var secondword = Console.ReadLine();
            Console.WriteLine(Abba(firstword, secondword));
            Console.ReadLine();
        }

        public static string Abba(string a, string b)
        {
            return a + b + b + a;
        }
    }
}
