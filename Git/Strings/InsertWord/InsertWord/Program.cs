﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InsertWord
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Provide outer text:");
            var outer = Console.ReadLine();
            Console.WriteLine("Provide inner text:");
            var inner = Console.ReadLine();
            Console.WriteLine(InsertWord(outer, inner));
            Console.ReadLine();
        }

        public static string InsertWord(string container, string word)
        {
            var o = container;
            var i = word;
            var o1 = o.Substring(0, 2);
            var o2 = o.Substring(2, 2);
            return o1 + i + o2;
        }
    }
}
