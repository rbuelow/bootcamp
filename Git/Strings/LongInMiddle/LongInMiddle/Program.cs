﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace LongInMiddle
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Provide a word:");
            var word1 = Console.ReadLine();
            Console.WriteLine("Provide a word of different length than the first word:");
            var word2 = Console.ReadLine();
            Console.WriteLine(InsertWord(word1, word2));
            Console.ReadLine();
        }

        public static string InsertWord(string container, string word)
        {
            if (container.Length > word.Length)
            {
                return word+container+word;
            }
            else if (container.Length == word.Length)
            {
                return "Both words have the same amount of characters";
            }
            else
                return container + word + container;
        }
    }
}
