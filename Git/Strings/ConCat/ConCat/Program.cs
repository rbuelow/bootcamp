﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConCat
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter first word:");
            var firstword = Console.ReadLine();
            Console.WriteLine("Enter second word:");
            var secondword = Console.ReadLine();
            var output = ConCat(firstword, secondword);
            
            
            
            
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static string ConCat(string a, string b)
        {
            var beginningofb = b.Substring(0, 1);
            var endofa = a.Substring(a.Length - 1, 1);

            if (endofa==beginningofb)
            {
                return String.Concat(a + b.Substring(1, b.Length-1));
            }
            else
            {
                return String.Concat(a+b);
            }
        }
    }
}
