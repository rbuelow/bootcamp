﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultipleEndings
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Provide a word:");
            var word = Console.ReadLine();
            Console.WriteLine(MultipleEndings(word));
            Console.ReadLine();
        }

        public static string MultipleEndings(string str)
        {
            
            var str1 = str.Substring(str.Length - 2, 2);
            return str1+str1+str1; 
        }
    }
}
