﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwapLast
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input a word");
            var input = Console.ReadLine();
            var output = SwapLast(input);
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static string SwapLast(string str)
        {
            var lastChar = str.Substring(str.Length - 1, 1);
            var secondLastChar = str.Substring(str.Length - 2, 1);
            var strbeginning = str.Substring(0, str.Length - 2);
            return strbeginning + lastChar + secondLastChar;
        }
    }
}
