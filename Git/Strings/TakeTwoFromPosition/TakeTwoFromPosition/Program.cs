﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TakeTwoFromPosition
{
    class Program
    {
        static void Main(string[] args)
        {
            //input from user
            Console.WriteLine("Please input a word of any length:");
            var word = Console.ReadLine();
            Console.WriteLine("Please enter a number:");
            var number = Console.ReadLine();

            //variable to assign output from method TakeTwoFromPosition
            var output = TakeTwoFromPosition(word, int.Parse(number));
            
            //output
            Console.WriteLine(Environment.NewLine + output);
            Console.ReadLine();
        }

        public static string TakeTwoFromPosition(string str, int n)
        {
            //if n is >= length of str, it will return first two characters of str. 
            //Else, it returns two characters starting at the n index
            if (n >= str.Length)
            {
                return str.Substring(0, 2);
            }
            else
            {
                return str.Substring(n, 2);
            }
        }
    }
}
