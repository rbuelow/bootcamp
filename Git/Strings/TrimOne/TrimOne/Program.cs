﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrimOne
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please input a word:");
            var input = Console.ReadLine();
            Console.WriteLine(TrimOne(input));
            Console.ReadLine();
        }

        public static string TrimOne(string str)
        {
            var strminusone = str.Length-2;
            var answer = str.Substring(1, strminusone);
            return answer;
        }
    }
}
