﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LastChars
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter first word:");
            var firstword = Console.ReadLine();
            Console.WriteLine("Please enter second word:");
            var secondword = Console.ReadLine();
            var output = LastChars(firstword, secondword);
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static string LastChars(string str, string str2)
        {
            var str2Index = str2.Length - 1;
            if (str.Length == 0 || str2.Length == 0)
            {
                if (str.Length == 0)
                {
                    str = "@";
                    return str + str2.Substring(str2Index, 1);
                }
                else if (str2.Length == 0)
                {
                    str2 = "@";
                    return str.Substring(0, 1) + str2;
                }
            }
            else
            {
                return str.Substring(0, 1) + str2.Substring(str2Index, 1);
            }
            return null;
        }
    }
}
