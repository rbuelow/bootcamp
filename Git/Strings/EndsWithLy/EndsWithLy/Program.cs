﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EndsWithLy
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please input a word:");
            var word = Console.ReadLine();
            Console.WriteLine("Does your word end with 'ly'?");
            Console.ReadLine();
            if (EndsWithLy(word) == true)
            {
                Console.WriteLine("True!!");
            }
            else
            {
                Console.WriteLine("False!!");
            }
            Console.ReadLine();
        }

        public static bool EndsWithLy(string str)
        {
            var str1 = str.Length - 2;
            var str2 = str.Substring(str1, 2);

            if (str2 == "ly")
            {
                return true;
            }
            else
                return false;
        }
    }
}
