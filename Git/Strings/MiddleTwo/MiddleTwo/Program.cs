﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MiddleTwo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input a word with an even amount of characters");
            var word = Console.ReadLine();
            if (word != null && word.Length%2 == 0)
            {
                Console.WriteLine(MiddleTwo(word));
            }
            else
            {
                Console.WriteLine("This word has an odd amount of characters");
            }
            Console.ReadLine();
        }

        public static string MiddleTwo(string str)
        {
            var str1 = str.Length/2;
            var str2 = str.Substring(str1 - 1, 2);
            return str2;
        }
    }
}
