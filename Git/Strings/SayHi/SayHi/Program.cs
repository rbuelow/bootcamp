﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SayHi
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter your name:");
            var nameinput = Console.ReadLine();
            var greet = SayHi(nameinput);
            Console.WriteLine(greet);
            Console.ReadLine();
        }

        public static string SayHi(string name)
        {
            var greeting = "Hello " + name + "!";
            return greeting;
        }
    }
}
