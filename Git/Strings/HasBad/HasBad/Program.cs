﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HasBad
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input a word with bad in it:");
            var word = Console.ReadLine();
            Console.WriteLine(HasBad(word));
            Console.ReadLine();
        }

        public static bool HasBad(string str)
        {
            var str1 = str.IndexOf("bad", 0);
            if (str1 == 0 || str1 == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
