﻿
using System.Collections.Generic;

namespace MyContacts.UI.Models
{
    public class Contact
    {
        public int ContactId { get; set; }
        public string Name { get; set; }
        public List<PhoneNumber> PhoneNumber { get; set; }
        public bool IsFriend { get; set; }
        
    }
}

