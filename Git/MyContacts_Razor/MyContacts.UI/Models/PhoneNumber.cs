﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyContacts.UI.Models
{
    public enum NumberType
    {
        Work,
        Home,
        Cell
    }
    public class PhoneNumber
    {
        public string Number { get; set; }
        public NumberType TypeOfNumber { get; set; }

    }
}
