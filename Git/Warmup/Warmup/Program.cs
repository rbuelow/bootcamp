﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Warmup
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, what is your name?");
            string name = Console.ReadLine();
            Console.WriteLine("Hello " + name + ", how old are you?");
            string age = Console.ReadLine();
            Console.WriteLine("You are " + age + " years old, " + name);
            Console.ReadLine();
        }
    }
}
