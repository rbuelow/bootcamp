﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SkipSum
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a number to add:");
            var num1 = Console.ReadLine();
            Console.WriteLine("Enter another number to add:");
            var num2 = Console.ReadLine();
            var output = SkipSum(int.Parse(num1), int.Parse(num2));
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static int SkipSum(int a, int b)
        {
            var result = 0;
            if (a + b >= 10 && a + b <= 19)
            {
                result = 20;
            }
            else
            {
                result = a + b;
            }
            return result;
        }
    }
}
