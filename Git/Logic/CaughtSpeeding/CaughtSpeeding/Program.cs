﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CaughtSpeeding
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What was your speed?");
            var speed = Console.ReadLine();
            Console.WriteLine("Is it your birthday? True or False");
            var torf = Console.ReadLine();
            var output = CaughtSpeeding(int.Parse(speed), Convert.ToBoolean(torf));
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static int CaughtSpeeding(int speed, bool isBirthday)
        {
            var result=0;

            if (isBirthday == true)
            {
                if (speed <= 65)
                {
                    result = 0;
                }
                else if (speed >= 66 && speed <= 85)
                {
                    result = 1;
                }
                else
                {
                    result = 2;
                }
            }
            else
            {
                if (speed <= 60)
                {
                    result = 0;
                }
                else if (speed >= 61 && speed <= 80)
                {
                    result = 1;
                }
                else
                {
                    result = 2;
                }
            }
            return result;
        }
    }
}
