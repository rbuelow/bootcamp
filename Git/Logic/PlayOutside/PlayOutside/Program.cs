﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace PlayOutside
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("What is the temp outside?");
            var degrees = Console.ReadLine();
            Console.WriteLine("Is it summer? True or False.");
            var isItSummer = Console.ReadLine();
            var output = PlayOutside(int.Parse(degrees), Convert.ToBoolean(isItSummer));
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static bool PlayOutside(int temp, bool isSummer)
        {
            bool areTheyPlaying;

            if (isSummer == false)
            {
                if (temp >= 60 && temp <= 90)
                {
                    areTheyPlaying = true;
                }
                else
                {
                    areTheyPlaying = false;
                }
            }
            else
            {
                if (temp >= 40 && temp <=100)
                {
                    areTheyPlaying = true;
                }
                else
                {
                    areTheyPlaying = false;
                }
            }

            return areTheyPlaying;
        }
    }
}
