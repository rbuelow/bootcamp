﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanHasTable
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("On a scale of 1-10, how stylish are you?");
            var style1 = Console.ReadLine();
            Console.WriteLine("How stylish is your date on a scale of 1-10?");
            var style2 = Console.ReadLine();
            var output = CanHazTable(int.Parse(style1), int.Parse(style2));
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static int CanHazTable(int yourStyle, int dateStyle)
        {
            var result=0;

            if (yourStyle >= 8 || dateStyle >= 8)
            {
                result = 2;
            } 
            else if (yourStyle <= 2 || dateStyle <= 2)
            {
                result = 0;
            }
            else
            {
                result = 1;
            }

            return result;
        }
    }
}
