﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GreatParty
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("How many cigars did the squirrels smoke?");
            var numOfCigars = Console.ReadLine();
            Console.WriteLine("Was the party on the weekend? True or False.");
            var isItWeekend = Console.ReadLine();
            var output = GreatParty(int.Parse(numOfCigars), Convert.ToBoolean(isItWeekend));
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static bool GreatParty(int cigars, bool isWeekend)
        {
            bool wasItaGreatParty;

            if (isWeekend == false)
            {
                if (cigars >= 40 && cigars <= 60)
                {
                    wasItaGreatParty = true;
                }
                else
                {
                    wasItaGreatParty = false;
                }
            }
            else
            {
                if (cigars >= 40)
                {
                    wasItaGreatParty = true;
                }
                else
                {
                    wasItaGreatParty = false;
                }
            }

            return wasItaGreatParty;
        }
    }
}
