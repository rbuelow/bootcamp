﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Make2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = new int [0];
            int[] arr2 = new[] {1, 2, 3};
            //Console.WriteLine(Make2(arr1, arr2));
            foreach (var c in Make2(arr1, arr2))
            {
                Console.WriteLine(c);   
            }
            Console.ReadLine();
        }

        public static int[] Make2(int[] a, int[] b)
        {
            //int aLength = a.Length;
            //int bLength = b.Length;
            //int cLength = aLength + bLength;
            int[] arr3 = new int[a.Length + b.Length];

            a.CopyTo(arr3, 0);
            b.CopyTo(arr3, a.Length);

            int[] arr4 = new int[2];

            for (int i = 0; i < 2; i++)
            {
                arr4[i] = arr3[i];
            }

            return arr4;
        }
    }
}
