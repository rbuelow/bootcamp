﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HigherWins
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = new[] {1, 4, 6, 34, 678};
            var output = HigherWins(arr1);
            foreach (int c in output)
            {
                Console.WriteLine(c);
            }
            Console.ReadLine();
        }

        public static int[] HigherWins(int[] numbers)
        {
            int[] newArr = new int[numbers.Length];
            Array.Sort(numbers);

            for (int i = 0; i < numbers.Length; i++)
            {
                newArr[i] = numbers[numbers.Length - 1];
            }
            return newArr;
        }
    }
}
