﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MakePi
{
    class Program
    {
        //Return an int array length n containing the first n digits of pi.

        //MakePi(3) -> {3, 1, 4}

        static void Main(string[] args)
        {
            Console.WriteLine("How many digits of pi do you want?");
            var input = Console.ReadLine();
            var output = MakePi(Int32.Parse(input));

            foreach (var c in output)
            {
                Console.WriteLine(c);
            }

            Console.ReadLine();
        }

        public static int[] MakePi(int n) 
        {
            int[] arr1 = new[] { 3, 1, 4, 1, 5, 9 };
            int[] newArr1 = new int[n];

            for (int i = 0; i < n; i++)
            {
                newArr1[i] = arr1[i];
            }
            return newArr1;
           
        }
    }
}
