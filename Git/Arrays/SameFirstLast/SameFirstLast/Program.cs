﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SameFirstLast
{
    class Program
    {

        //Given an array of ints, return true if the array is length 1 or more, and the first element and the last element are equal. 

        //SameFirstLast({1, 2, 3}) -> false
        //SameFirstLast({1, 2, 3, 1}) -> true
        //SameFirstLast({1, 2, 1}) -> true

        static void Main(string[] args)
        {
            int[] arr1 = new[] {1, 2, 5, 8, 2};

            var output = SameFirstLast(arr1);
            
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static bool SameFirstLast(int[] numbers) 
        {
            if (numbers[0] == numbers[4])
            {
                return true;
            } else  return false;
        }
    }
}
