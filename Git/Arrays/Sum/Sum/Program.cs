﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sum
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = new[] {1, 3, 7, 9, 5, 3};

            var output = Sum(arr1);
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static int Sum(int[] numbers)
        {
            var intSum = 0;
            for (int i = 0; i < numbers.Length; i++)
            {
                intSum += numbers[i];
            }
            return intSum;
        }
    }
}
