﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonEnd
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = new[] {2, 5, 8, 1};
            int[] arr2 = new[] {5, 9, 3, 1};

            var output = CommonEnd(arr1, arr2);
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static bool CommonEnd(int[] a, int[] b)
        {
            if (a[0] == b[0] || a[a.Length-1] == b[b.Length-1])
            {
                return true;
            }
            else return false;
        }
    }
}
