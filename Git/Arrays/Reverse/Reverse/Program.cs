﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reverse
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = new[] {1, 2, 3, 4, 5, 6};
            int[] output = Reverse(arr1);
            foreach (int c in output)
            {
                Console.WriteLine(c);
                
            }
            Console.ReadLine();
        }

        public static int[] Reverse(int[] numbers)
        {
            int[] reversedArr = new int[numbers.Length];
            int counter = 0;

            for (int i = numbers.Length - 1; i >= 0; i--)
            {
                reversedArr[counter] = numbers[i];
                counter++;
            }
            return reversedArr;
        }
    }
}
