﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fix23
{
    class Program
    {
        //Given an int array length 3, if there is a 2 in the array immediately followed by a 3, set the 3 element to 0. 
        //Return the changed array. 

        //Fix23({1, 2, 3}) ->{1, 2, 0}
        //Fix23({2, 3, 5}) -> {2, 0, 5}
        //Fix23({1, 2, 1}) -> {1, 2, 1}
        static void Main(string[] args)
        {
            int[] arr1 = new[] {2, 3, 4};
            var output = Fix23(arr1);
            foreach (var c in output)
            {
                Console.WriteLine(c);
                
            }
            Console.ReadLine();

        }

        public static int[] Fix23(int[] numbers)
        {
            foreach (int i in numbers)
            {
                if (i == 2)
                {
                    var indexOf2 = Array.IndexOf(numbers, i);
                    if (numbers[indexOf2 + 1] == 3)
                    {
                        var indexOf3 = indexOf2 + 1;
                        numbers[indexOf3] = 0;
                    }
                    else
                    {
                        continue;
                    }
                }
            }
            return numbers;
        }
    }
}
