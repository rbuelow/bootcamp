﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RotateLeft
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = new[] {1, 2, 3, 4, 5, 6, 7};
            int[] arr2 = new[] {5, 11, 9};
            int[] arr3 = new[] {7, 0, 0};

            var output1 = RotateLeft(arr1);

            foreach (var c in output1)
            {
                Console.WriteLine(c);
            }

            Console.ReadLine();
        }

        public static int[] RotateLeft(int[] numbers)
        {
            int[] returnArr = new int [numbers.Length];
            for (int i = 0; i < numbers.Length - 1; i++)
            {
                returnArr[i] = numbers[i + 1];
            }
            returnArr[numbers.Length - 1] = numbers[0];
            return returnArr;
        }
    }
}
