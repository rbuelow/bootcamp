﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeepLast
{
    class Program
    {
        //Given an int array, return a new array with double the length where its last element is the same as the original array, 
        //and all the other elements are 0. The original array will be length 1 or more. Note: by default, a new int array contains all 0's. 

        //KeepLast({4, 5, 6}) -> {0, 0, 0, 0, 0, 6}
        //KeepLast({1, 2}) -> {0, 0, 0, 2}
        //KeepLast({3}) -> {0, 3}

        static void Main(string[] args)
        {
            int[] arr1 = new[] {1, 2, 3, 4};

            var output = KeepLast(arr1);
            foreach (var c in output)
            {
                Console.WriteLine(c);
            }
            Console.ReadLine();
        }

        public static int[] KeepLast(int[] numbers)
        {
            int[] newArr = new int[numbers.Length * 2];
            newArr[numbers.Length*2 - 1] = numbers[numbers.Length - 1];
            return newArr;
        }
    }
}
