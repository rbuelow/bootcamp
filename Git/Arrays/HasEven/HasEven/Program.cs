﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HasEven
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = new[] {1, 3, 5, 7, 8};
            var output = HasEven(arr1);
            Console.WriteLine(output);
            Console.ReadLine();
        }

        public static bool HasEven(int[] numbers)
        {
            bool answer = new bool();
            foreach (int t in numbers)
            {
                if (t%2 == 0)
                {
                    answer = true;
                }
                else continue;
            }
            return answer;
        }
    }
}
