﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RyanNickMasteryProject.Models;
using RyanNickMasteryProject.Repositories;

namespace RyanNickMasteryProject.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
  
        public ActionResult AdminDashboard()
        {
            return View();
        }

        public ActionResult UserDetails(int id)
        {
            var user = AdminRepo.GetLmsUserById(id);
            return View(user);
        }

    }
}