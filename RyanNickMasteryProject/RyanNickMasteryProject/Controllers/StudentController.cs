﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RyanNickMasteryProject.BizLogic;
using RyanNickMasteryProject.Models;
using RyanNickMasteryProject.Models.CourseModels;
using RyanNickMasteryProject.Repositories;

namespace RyanNickMasteryProject.Controllers
{
    public class StudentController : Controller
    {
        // GET: Student
        public ActionResult StudentGrades(string courseName, int userId)
        {
            var id = StudentRepo.GetCourseIdByCourseName(courseName);
            var course = new StudentCourse
            {
                UserId = userId
            };
            var studentAssignment = new RyanNickMasteryProject.Models.CourseModels.Assignment();
            var assignments = StudentRepo.GetAssignmentNamePercentageScoredAndLetterGrade(userId, id);
            foreach (var x in assignments)
            {
                x.AssignmentName = studentAssignment.AssignmentName;
                x.Percentage = studentAssignment.Percentage;
                x.Grade = studentAssignment.LetterGrade;
                
                course.StudentAssignments.Add(studentAssignment);
            }
            

            return View(course);
        }

        public ActionResult StudentDashboard(int id)
        {
            StudentOperations ops = new StudentOperations();

            var student = AdminRepo.GetLmsUserById(id);
            
            return View(student);
        }
    }
}