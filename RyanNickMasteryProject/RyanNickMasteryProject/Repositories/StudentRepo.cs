﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.WebSockets;
using Microsoft.Owin.Security.Provider;
using RyanNickMasteryProject.Models;
using RyanNickMasteryProject.Models.CourseModels;


namespace RyanNickMasteryProject.Repositories
{
    public class StudentRepo
    {
        public static List<LmsUserSelectCourseNameAndCurrentGrade_Result> GetCourseAndGrade(int userId)
        {
            using (var db = new SWC_LMSEntities())
            {
                var results = db.LmsUserSelectCourseNameAndCurrentGrade(userId).ToList();
                return results;
            }
        }

        public static List<LmsUserAndCourseIdSelectGradesForCourse_Result>
            GetAssignmentNamePercentageScoredAndLetterGrade(int userId, int courseId)
        {
            using (var db = new SWC_LMSEntities())
            {
                var results = db.LmsUserAndCourseIdSelectGradesForCourse(userId, courseId).ToList();
                return results;
            }   
        }

        public static List<StudentCourse> GetStudentCourses(int userId)
        {
            var db = new SWC_LMSEntities();
            List<StudentCourse> courseList = new List<StudentCourse>();
            var courses = db.GetLmsUserCourses(userId).ToList();

            foreach (var x in courses)
            {
                StudentCourse c = new StudentCourse();
                c.CurrentGrade = x.CurrentGrade;
                c.CourseName = x.CourseName;
                
                courseList.Add(c);
            }
            return courseList;
        }

        public static int GetCourseIdByCourseName(string courseName)
        {
            var db = new SWC_LMSEntities();
            var id = from c in db.Courses
                where c.CourseName == courseName
                select c.CourseId;


            return id.FirstOrDefault();
        }


    }
}